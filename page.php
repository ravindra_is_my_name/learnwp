<?php get_header(); ?>
<div class="container-fluid mt-2">
    <div class="container custom-tbl p-4">
        <h2 class="text-center font-weight-bold mdb-color-text font-noto-sans"><?php the_title(); ?></h2>
        <?php if (have_posts()) : while (have_posts()) : the_post();
                the_content();
            endwhile;
        ?>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>