<?php get_header(); ?>
<div class="container-fluid mb-4">
    <div class="row">
        <div id="carouselExampleIndicators" class="carousel slide p-0" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">

                <?php
                $args = array(
                    'post_type' => 'slider',
                    'posts_per_page' => 4,
                );
                $latest = new WP_Query($args);
                if ($latest->have_posts()) {
                    $i=0;
                    while ($latest->have_posts()) : $latest->the_post();
                    $i++;
                ?>
                        <div class="<?php if($i==1) echo 'active' ?> carousel-item">

                            <?php if (has_post_thumbnail()) : ?>
                                <div class="text-center mb-4">
                                    <img  height="500px" width="100%" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                                </div>
                            <?php endif; ?>
                            <!-- <img class="" height="500px" width="100%" src="https://cdn.pixabay.com/photo/2018/07/31/14/09/hot-3575167_960_720.jpg" class="d-block w-100" alt="..."> -->
                        </div>
                <?php
                    endwhile;
                    wp_reset_postdata();
                }
                ?>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</div>
<?php get_footer(); ?>