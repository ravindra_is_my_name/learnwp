<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn WP</title>
    <?php wp_head(); ?>
</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-start">
            <div class="brand-logo">
                <?php if (has_custom_logo()) :
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $image = wp_get_attachment_image_src($custom_logo_id, 'large');
                ?>
                    <a href="<?php echo esc_url(home_url()); ?>">
                        <img class="img-fluid" src="<?php echo $image['0']; ?>" alt="" style="max-height: 120px;">
                    </a>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-nav">
        <nav class="navbar container navbar-expand-lg bg-nav">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#primarymenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php
            wp_nav_menu(array(
                'theme_location'  => 'primary',
                'depth'           => 2,
                'container'       => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id'    => 'primarymenu',
                'menu_class'      => 'navbar-nav  justify-content-left w-100 primary-menu',
                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                'walker'          => new WP_Bootstrap_Navwalker(),
            ));
            ?>
        </nav>
    </div>